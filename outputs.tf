output "load_balancer_dns_name" {
  value = join("", aws_lb.load_balancer.*.dns_name)
}

output "load_balancer_zone_id" {
  value = join("", aws_lb.load_balancer.*.zone_id)
}

output "load_balancer_arn_suffix" {
  value = join("", aws_lb.load_balancer.*.arn_suffix)
}

output "load_balancer_arn" {
  value = join("", aws_lb.load_balancer.*.arn)
}

output "load_balancer_id" {
  value = join("", aws_lb.load_balancer.*.id)
}

output "load_balancer_target_group_arn" {
  value = join("", aws_lb_target_group.target_group.*.arn)
}

output "load_balancer_listener_arn" {
  value = join("", aws_lb_listener.lb_tls_listener.*.arn)
}