# load-balancer module

## Prerequisites

- Route53 hosted zone

## Created resources

- Route53 alias recordset pointing to Network Load Balancer (NLB)
- An NLB with:
  - 2 listeners: TCP-listener on port 80 and TLS-listener on port 443
  - A target group of type "IP"
  - Both listeners point to the same target group
- For the TLS-listener either a provided certificate can be used - or - a new wildcard certificate in ACM can be created if requested
