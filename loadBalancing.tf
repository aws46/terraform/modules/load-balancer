resource "aws_lb" "load_balancer" {
  count = var.is_enabled ? 1 : 0

  name               = join("-", [local.name, var.names.load_balancer])
  internal           = var.internal
  load_balancer_type = var.load_balancer_type//"network"
  subnets            = var.subnets
  tags = var.tags
}

resource "aws_lb_target_group" "target_group" {
  count = var.is_enabled ? 1 : 0

  depends_on = [aws_lb.load_balancer]

  name        = join("-", [local.name, var.names.lb_target_group])
  port        = var.target_group_port
  protocol    = local.protocol
  target_type = "ip"
  vpc_id      = var.vpc_id

  #https://github.com/terraform-providers/terraform-provider-aws/issues/9093#issuecomment-556945481
/*  dynamic "stickiness" {
    for_each = var.use_alb_sticky_sessions ? ["use_sticky"] : []
    content {
      type            = "lb_cookie"
      cookie_duration = 86400
      enabled = var.use_alb_sticky_sessions
    }
  }*/

  stickiness {
    enabled = var.is_alb_sticky_sessions_used
    type = local.stickiness_type
  }

  tags = var.tags
}

### PUBLIC DNS ###
data "aws_route53_zone" "public_route53_zone" {
  count = var.is_route_53_zone_enabled ? 1 : 0

  name = var.public_hosted_zone_name
}

# https://www.terraform.io/docs/providers/aws/r/acm_certificate.html
resource "aws_acm_certificate" "lb_tls_cert" {
  count = var.is_route_53_zone_enabled ? ( var.is_existing_cert_arn_used ? 0 : 1) : 0

  domain_name               = join(".", ["*", var.public_hosted_zone_name])
  validation_method         = "DNS"
  subject_alternative_names = [var.public_hosted_zone_name]
  tags                      = var.tags

  lifecycle {
    create_before_destroy = true
  }
}

# Create CNAME record in Route53 hosted zone necessary for DNS validation of the created certificate
resource "aws_route53_record" "dnsvalidation_with_hostedzone" {
  count = var.is_route_53_zone_enabled ? ( var.is_existing_cert_arn_used ? 0 : 1) : 0

  allow_overwrite = true
  name            = aws_acm_certificate.lb_tls_cert[count.index].domain_validation_options[0].resource_record_name
  type            = aws_acm_certificate.lb_tls_cert[count.index].domain_validation_options[0].resource_record_type
  zone_id         = join("", data.aws_route53_zone.public_route53_zone.*.id)
  records         = [aws_acm_certificate.lb_tls_cert[count.index].domain_validation_options[0].resource_record_value]
  ttl             = 60
}

# DNS Validation of the created certificate
resource "aws_acm_certificate_validation" "certvalidation_with_hostedzone" {
  count = var.is_route_53_zone_enabled ? ( var.is_existing_cert_arn_used ? 0 : 1) : 0

  certificate_arn         = aws_acm_certificate.lb_tls_cert[count.index].arn
  validation_record_fqdns = [aws_route53_record.dnsvalidation_with_hostedzone[count.index].fqdn]
}

# Route53 Alias record for the Load Balancer
module "rule_engine_public_route53_alias" {
  enabled = var.is_route_53_zone_enabled

  source         = "git::https://github.com/cloudposse/terraform-aws-route53-alias.git?ref=tags/0.8.2"
  aliases        = [var.public_hosted_zone_alias]
  parent_zone_id = join("", data.aws_route53_zone.public_route53_zone.*.id)

  target_dns_name = join("", aws_lb.load_balancer.*.dns_name)
  target_zone_id  = join("", aws_lb.load_balancer.*.zone_id)
}

resource "aws_lb_listener" "lb_tls_listener" {
  count = var.is_enabled ? 1 : 0

  load_balancer_arn = join("", aws_lb.load_balancer.*.arn)
  port              = local.protocol == "TLS" ? 443 : 80
  protocol          = local.protocol
  ssl_policy        = local.ssl_policy
  certificate_arn   = var.is_route_53_zone_enabled ? (var.is_existing_cert_arn_used ? var.cert_arn : aws_acm_certificate.lb_tls_cert[count.index].arn) : var.cert_arn

  default_action {
    type             = "forward"
    target_group_arn = join("", aws_lb_target_group.target_group.*.arn)
  }
}