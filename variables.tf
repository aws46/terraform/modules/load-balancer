# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED MODULE PARAMETERS - YOU MUST PASS THOSE TERRAFORM PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "vpc_id" {
  type = string
}

variable "public_hosted_zone_name" {
  type = string
  default = ""
}

variable "subnets" {
  type = list(string) #Private subnet1 id
}

variable "internal" {
  type = bool
}

variable "load_balancer_type" {
  type = string
}

variable "public_hosted_zone_alias" {
  description = "ALIAS name in Route 53 to point to the Network Load Balancer"
  type        = string
  default = ""
}

variable "environment" {
  type = string
}

variable "service_name" {
  type = string
}

variable "target_group_port" {
  type = number
  default = 80
}

variable "tags" {
  type = map(string)
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL MODULE PARAMETERS - YOU MAY CHANGE THESE DEFAULTS
# ---------------------------------------------------------------------------------------------------------------------

variable "is_enabled" {
  type    = bool
  default = true
}

variable "is_route_53_zone_enabled" {
  description = "It enables the route53 alias record and certificate for the load balancer"
  type = bool
  default = false
}

variable "is_existing_cert_arn_used" {
  description = "Whether this should use an existing certificate arn. If not provided, it will provision the load balancer's TLS listener using an AWS generated certificate"
  type        = bool
  default     = false
}

variable "cert_arn" {
  description = "The ARN of the certificate to use with the LB, utilised only if use_existing_cert_arn is true"
  type        = string
  default     = null
}

variable "is_alb_sticky_sessions_used" {
  description = "Possible only with an ALB. This cannot be used with CLBs or NLBs."
  type        = bool
  default     = false
}

variable "names" {
  type = map(string)
  default = {
    load_balancer   = "lb"
    lb_target_group = "lb-tg"
  }
}

locals {
  name = var.service_name == null ? join("-", [var.environment]) : join("-", [var.service_name, var.environment])
  protocol = var.load_balancer_type == "application" ? "HTTP" : (var.is_route_53_zone_enabled ? "TLS" : "TCP")
  ssl_policy = var.load_balancer_type == "network" && local.protocol == "TLS"? "ELBSecurityPolicy-2016-08" : null
  stickiness_type = local.protocol == "HTTP" ? "lb_cookie" : "source_ip"
}
