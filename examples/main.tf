###########################################################
# Example demonstrating how to use the load-balancer module
###########################################################

terraform {
  required_version = ">= 0.12.0"

  required_providers {
    aws      = "~> 2.70"
    local    = "~> 1.2"
    template = "~> 2.1"
  }

}

provider "aws" {
  region = "eu-west-1"
}

locals {
  common_tags = {
    Name             = "tf-test-nlb"
    ManagedBy        = "Terraform"
  }
}

module "test_network_load_balancer" {
  source                   = "../"
  vpc_id                   = var.vpc_id
  subnets                  = var.public_zone_subnet_ids
  internal                 = false
  public_hosted_zone_name  = var.public_hosted_zone_name
  public_hosted_zone_alias = var.public_hosted_zone_alias
  is_existing_cert_arn_used    = var.use_existing_cert_arn
  cert_arn                 = var.cert_arn
  environment              = lower(var.pipeline_environment)
  tags                     = local.common_tags
}
