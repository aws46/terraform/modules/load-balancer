variable "pipeline_environment" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "public_zone_subnet_ids" {
  type = list(string)
}

variable "public_hosted_zone_name" {
  type = string
}

variable "public_hosted_zone_alias" {
  type = string
}

variable "use_existing_cert_arn" {
  type = bool
}

variable "cert_arn" {
  type = string
}
