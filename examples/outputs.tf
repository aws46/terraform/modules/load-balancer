output "lb_dns_name" {
  value = module.test_network_load_balancer.load_balancer_dns_name
}
